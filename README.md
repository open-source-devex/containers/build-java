[![pipeline status](https://gitlab.com/open-source-devex/containers/build-java/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/containers/build-java/commits/master)

# Docker container to run builds of java projects

Based on `registry.gitlab.com/open-source-devex/containers/build:latest` and `anapsix/alpine-java:8_jdk-dcevm_unlimited` with tooling for building projects during CI.

Credits for installing glibc and JDK 8 on Alpine are due to the [authors of `anapsix/alpine-java:8_jdk-dcevm_unlimited`](https://github.com/anapsix/docker-alpine-java/graphs/contributors).
All we do here is a multi-stage docker build to copy glibc and JDK from `anapsix/alpine-java:8_jdk-dcevm_unlimited` to a new image based on `registry.gitlab.com/open-source-devex/containers/build:latest`.
This seemed at this time the best way to extend our Alpine base `build` image with JDK 8.
